package com.example.zhangwei.jtviewplayerandroid;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AnimationUtils;
import android.widget.ViewFlipper;

/**
 * Created by zhangwei on 16/3/28.
 */
public class PlayController extends ViewFlipper implements GestureDetector.OnGestureListener {

    private static final int FLING_MIN_DISTANCE = 100;
    private GestureDetector detector;
    private Message msg;
    private final int FLIP_MSG = 1;
    private int playInteval;

    protected void setplayInteval(int playInteval){
        this.playInteval = playInteval;
    }
    protected  int getPlayInteval(){
        return this.playInteval;
    }
    public PlayController(Context context) {
        super(context);

        System.out.println("运行到JTAHuaDong的构造方法了！");
        // 注册一个GestureDetector
        detector = new GestureDetector(this);

        msg = mHandler.obtainMessage(FLIP_MSG);
        mHandler.sendMessageDelayed(msg, playInteval);
    }

    /**
     * 接收到消息，就显示下一个View。
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == FLIP_MSG) {
                rightToLeft();
                showNext();
                msg = obtainMessage(FLIP_MSG);
                sendMessageDelayed(msg, playInteval);
            }
        }
    };

    /**
     * 这个方法必须要写，主要就是用的这个方法的返回值。
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // 将触屏事件交给手势识别类处理
        return this.detector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        System.out.println("执行到onDown！！！！停止计时器");
        stopFlipping();
        this.setAutoStart(false);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        System.out.println("执行到onShowPress！！！！");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        System.out.println("执行到onSingleTapUp了！！！！");
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        System.out.println("执行到onLongPress！！！！");
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {
        System.out.println("正在滑动！！！！！");
        if (e1.getX() - e2.getX() > FLING_MIN_DISTANCE) {

            rightToLeft();
            this.showNext();
            mHandler.removeMessages(FLIP_MSG);
            msg = mHandler.obtainMessage(FLIP_MSG);

            mHandler.sendMessageDelayed(msg, playInteval);
            return true;
        }
        if (e1.getX() - e2.getX() < -FLING_MIN_DISTANCE) {

            leftToRight();
            this.showPrevious();

            mHandler.removeMessages(FLIP_MSG);
            msg = mHandler.obtainMessage(FLIP_MSG);
            mHandler.sendMessageDelayed(msg, playInteval);
            return true;
        }
        return false;
    }

    //正向切换
    private void rightToLeft(){
        //设置View进入和退出的动画效果
        this.setInAnimation(AnimationUtils.loadAnimation(this.getContext(),
                com.example.zhangwei.jtviewplayerandroid.R.anim.left_in));
        this.setOutAnimation(AnimationUtils.loadAnimation(this.getContext(),
                com.example.zhangwei.jtviewplayerandroid.R.anim.left_out));
    }
    //反向切换
    private void leftToRight(){
        //设置View进入和退出的动画效果
        this.setInAnimation(AnimationUtils.loadAnimation(this.getContext(),
                com.example.zhangwei.jtviewplayerandroid.R.anim.right_in));
        this.setOutAnimation(AnimationUtils.loadAnimation(this.getContext(),
                com.example.zhangwei.jtviewplayerandroid.R.anim.right_out));
    }

}
