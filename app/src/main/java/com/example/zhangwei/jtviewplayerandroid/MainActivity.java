package com.example.zhangwei.jtviewplayerandroid;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView[] ivs = new ImageView[4];
        ivs[0] = new ImageView(this);
        ivs[0].setImageResource(R.drawable.pic1);
        ivs[1] = new ImageView(this);
        ivs[1].setImageResource(R.drawable.pic2);
        ivs[2] = new ImageView(this);
        ivs[2].setImageResource(R.drawable.pic3);
        ivs[3] = new ImageView(this);
        ivs[3].setImageResource(R.drawable.pic4);

        int viewHeight = 400;

        int playInteval = 3000;
        JTViewPlayer jtViewPlayer = new JTViewPlayer(this, ivs, viewHeight, playInteval);
        RelativeLayout.LayoutParams jtaParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,500);
        this.addContentView(jtViewPlayer, jtaParams);
    }
}
