package com.example.zhangwei.jtviewplayerandroid;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

/**
 * Created by zhangwei on 16/3/28.
 */
public class JTViewPlayer extends ViewGroup{

    private int playInteval;
    private RadioGroup rg;

    public void setPlayInteval(int playInteval) {
        this.playInteval = playInteval;
    }

    public int getPlayInteval() {
        return playInteval;
    }

    public JTViewPlayer(Context context, View[] views, int viewHeight, int playInteval) {
        super(context);

        this.playInteval = playInteval;
        //创建添加PlayController控件。
        PlayController playController = new PlayController(this.getContext());
        //设置PlayController控件的宽高布局，让他的宽自适应屏幕的宽。
        playController.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        playController.setplayInteval(this.getPlayInteval());

        //添加RadioGroup控件
        rg = new RadioGroup(context);
        //设置RadioGroup中子控件的布局为水平布局。
        rg.setOrientation(LinearLayout.HORIZONTAL);
        //设置RadioGroup自己的大小，宽度和高度都自适应内容。
        rg.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        //循环向控制器中添加子View，就是不知道能不能直接添加一个数组或者集合，这样的话就不需要遍历了。
        //同时添加相应数量的RadioButton也就是小圆点。
        for( View view : views){
            playController.addView(view);
            RadioButton rb = new RadioButton(context);
            rb.setText(null);
            rg.addView(rb);
        }

        this.addView(playController);
        this.addView(rg);
    }

    /**
     * 本方法用来确定子view在当前ViewGroup中的布局。
     * @param changed 不知道是干啥用的。。。
     * @param l view 左 边框距容器 左 边框距离
     * @param t view 上 边框距容器 上 边框距离
     * @param r view 右 边框距容器 左 边框距离
     * @param b view 下 边框距容器 上 边框距离
     */
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        PlayController huadong = (PlayController) getChildAt(0);
        //huadong.setplayInteval(playInteval);
        huadong.layout(l, t, r, b);

        RadioGroup rg = (RadioGroup) getChildAt(1);
        int rgl = (r-rg.getMeasuredWidth())/2;
        int rgt = b-rg.getMeasuredHeight();
        int rgr = rgl + rg.getMeasuredWidth();
        int rgb = b;
        rg.layout(rgl, rgt, rgr, rgb);

        //得到正在显示的子view在父级容器中的索引。
        int index = huadong.getDisplayedChild();
        ((RadioButton) rg.getChildAt(index)).setChecked(true);
    }

    /**
     * 计算子控件的尺寸，由于子控件的尺寸可能会改变，所以每次改变时，都会调用本方法重新计算。
     * 如果加载的子View中含有一个控件组合（比如子View中还包含孙子View），则必须重写本方法，否则孙子控件不会被显示。
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
