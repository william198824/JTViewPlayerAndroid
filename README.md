### 使用说明：

JTViewPlayerAndroid是一个安卓版本的视图轮播控件。这也是我最为一个新手，第一次发上来的控件。从一点不懂，到完成并且优化他，一共花了我两天的时间，让大神们见笑了:sweat_smile:以后我也会不断的完善它，并且有时间的话也会做出一个IOS版本，嗯，就叫做JTViewPlayerSwift吧！

核心特点：

- 这里是列表文本
- 新手写就，代码简单通俗易懂，方便新新手理解逻辑，和维护修改。
- 采用延迟消息机制，只占一个线程，效率还是蛮高的.......吧:kissing_smiling_eyes:
- 控件都是代码生成的，不需要在xml上布局，想加在哪就copy到哪。
- 控件高度可调，自动向左滑动并且周期可调。
- 支持点击事件（在PlayController中修改onSingleTapUp方法体即可。 
- 全中文的文档和注释:v:

使用方法：

1. 在你的项目中引入com.example.zhangwei.jtviewplayerandroid包。
1. 把我res中的anim文件夹引入你的res。
1. PlayerController类是控制滑动效果的核心类，可以不用管。在使用时，直接实例化JTViewPlayer就行了，这里的代码可参考我的mainActivity类。

    **JT是江豚的缩写，是长江里独有的类似海豚的可爱动物，特点是每时每刻都在微笑，离完全灭绝仅剩15年！[点击进入详情](http://baike.baidu.com/link?url=PBmqE0kHjHRa1Z4Y1Z-H2e-i5NWzQfSlI1giNML5_N5AHj2pGm3aovG2bOTeqUwxvDszh8V33ytaQJiytbadtq)**